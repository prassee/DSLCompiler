package compiler

import scala.io.Source
import org.scalatest.Matchers

class DSLParseSpec extends org.scalatest.WordSpec
    with org.scalatest.Matchers
    with NSSamplesFixture
    with TestCommons {

  val splCompiler = compiler.DSLCompiler
  "DSL Compiler" when {

    "given a Namespace definition" should {

      "Parse the namespace definition to NameSpaceDef" in {
        val x = splCompiler.parse(singleLineNSDefinition)
        assertParseResult(x, NameSpaceDef("id", "IDENTITY", None, Option("SOLR")))
      }

      "Parse the unparsed namespace definition" in {
        val x = splCompiler.parse(unparsedDef)
        assertParseResult(x, (NameSpaceDef("unparsed", "Unparsed Data", Option("UNPARSED"), None)))
      }

      "Parse multilined namespace definition" in {
        val mlGrammar = this.getClass.getResource("/multiLineGrammar.spl").getPath
        val file = Source.fromFile(mlGrammar)
        val x = splCompiler.parse(file.mkString)
        assertParseResult(x, (NameSpaceDef("id", "IDENTITY", None, Option("SOLR"), Option("""/^\s*\=+\s+IDENTITY/"""))))
      }

      "Parse many Namespace Definitions in a single file" in {
        val mlGrammar = this.getClass.getResource("/mutiNameSpace.spl").getPath
        val file = Source.fromFile(mlGrammar)
        val nsArray = file.mkString.split(";\n")
        val nsObjects = nsArray.map(ns => splCompiler.parse(ns))
      }

      "Fail if name is not followed by namespace kw" in {
        ???
      }

      "Fail if desc is not followed by desc kw" in {
        ???
      }

      "Fail if def does not end with SOLR" in {
        ???
      }

    }

    "given a table definition" should {

    }

  }
}

trait NSSamplesFixture {
  val singleLineNSDefinition = """DEFINE NAMESPACE id DESCRIPTION "IDENTITY" SOLR
    """.stripMargin
  val unparsedDef = """DEFINE NAMESPACE unparsed DESCRIPTION "Unparsed Data" TYPE UNPARSED
    """.stripMargin

  /**
   * multiline
   */
  val completeNSDef = "\\n" +
    "DEFINE NAMESPACE id DESCRIPTION \"IDENTITY\" SOLR \\r\\n" +
    "BEGINS WITH \"IDENTITY\" \\r\\n" +
    ";"
}

trait TestCommons extends Matchers {
  def assertParseResult(pr: SPLParseResult, expected: NameSpaceDef) = {
    pr match {
      case nsdf: NameSpaceDef  => nsdf shouldBe (expected)
      case spl: SPLParseFailed => spl shouldBe (expected)
    }
  }
}
