package compiler

import scala.collection.mutable.ListBuffer

case class NSNode[T](str: String, t: T) {
  private val child = ListBuffer[NSNode[T]]()
  def hasChild = child.isEmpty
  def addChild(node: NSNode[T]) = child.+=(node)
  def getChildren = child
}

class TreeBuilder[T](seed: T) {

  private val root = NSNode("root", seed)

  private def addTopLevelNode(node: NSNode[T]) =
    root.addChild(node)

  private def getChild(name: String) =
    root.getChildren.filter { x => x.str.equals(name) }.head

  def addNode(node: NSNode[T]) {
    if (!node.str.contains(".")) {
      addTopLevelNode(node)
    } else {
      val x = node.str.split("\\.")
      findTargetParent(getChild(x.head), x.tail.toList, node)
    }
  }

  def buildTree = root

  private def findTargetParent(x: NSNode[T], list: List[String], toAdd: NSNode[T]) = {
    def iter(parent: NSNode[T], list: List[String]) {
      list match {
        case h :: Nil => parent.addChild(toAdd)
        case h :: t   => iter(parent.getChildren.filter { x => x.str.equals(h) }.head, list.tail)
      }
    }
    iter(x, list)
  }

}
