package compiler

import scala.util.parsing.combinator.syntactical._
import scala.util.parsing.combinator.lexical.Lexical

/**
 * DSL Compiler
 */
object DSLCompiler extends StandardTokenParsers {

  lexical.delimiters ++= List(" ", ";")
  lexical.reserved += ("DEFINE", "NAMESPACE", "DESCRIPTION", "TYPE", "BEGINS", "ENDS", "WITH")

  private def parseNSName = "DEFINE" ~ "NAMESPACE" ~ ident ^^ { case "DEFINE" ~ "NAMESPACE" ~ i => i }

  private def parseNSDesc = "DESCRIPTION" ~ stringLit ^^ { case "DESCRIPTION" ~ s => s }

  private def parserNSType: Parser[String] = ("TYPE" ~ ident) ^^ { case "TYPE" ~ i => i }

  private def parseREFType = ("REF" ~ stringLit) ^^ { case "REF" ~ s => s }

  private def parseTYPEorREF = (parserNSType | parseREFType) ^^ { case i => i }

  private def parseSolrType = (ident) ^^ { case i => i }

  private def parseBeginsWith = ("BEGINS" ~> "WITH" ~> stringLit) ^^ { case regex => regex }

  private def parseEndsWith = ("ENDS" ~> "WITH" ~> stringLit) ^^ { case regex => regex }

  def compileNameDesc =
    parseNSName ~ parseNSDesc ~ opt(parseTYPEorREF) ~ opt(parseSolrType) ~ opt(parseBeginsWith) ~ opt(parseEndsWith) ^^ {
      case n ~ d ~ t ~ s ~ bw ~ ew => NameSpaceDef(n, d, t, s, bw, ew)
    }

  def parse(spl: String): SPLParseResult = {
    compileNameDesc(new lexical.Scanner(spl)) match {
      case Success(e, _) => e
      case Failure(f, _) => SPLParseFailed(f)
      case Error(e, _)   => SPLParseFailed(e)
    }
  }

}

trait SPLParseResult

case class NameSpaceDef(nsName: String,
                        desc: String, nsType: Option[String] = None, isSolr: Option[String] = None,
                        bwRegex: Option[String] = None, ewRegex: Option[String] = None) extends SPLParseResult

case class SPLParseFailed(msg: String) extends SPLParseResult                        
