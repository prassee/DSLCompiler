package compiler

import scala.io.Source
import java.io.File

object NodeConverisons {
  implicit def toNSNode(nameSpaceDef: NameSpaceDef): NSNode[NameSpaceDef] =
    NSNode(nameSpaceDef.nsName, nameSpaceDef)
}

object SPLCompilerApp {

  private val treeBuilder = new TreeBuilder[NameSpaceDef](NameSpaceDef("root", "root"))

  private def compileFn(x: String) = DSLCompiler.parse(x)

  private def addToTree(pr: SPLParseResult) {
    import NodeConverisons._
    pr match {
      case nsDef: NameSpaceDef  => treeBuilder.addNode(nsDef)
      case fail: SPLParseFailed => throw new Exception("Parse failed")
    }
  }

  def parseSPLFile(filePath: String) = {
    val compileAddToTree = (compileFn _ andThen addToTree _)
    val splFile = Source.fromFile(filePath).mkString.split(";\\n")
    splFile.foreach { nsDef => compileAddToTree(nsDef) }
    treeBuilder.buildTree
  }

}