name := "spikeSPLParser"

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
	"org.tpolecat" %% "atto-core"  % "0.4.2",
	"com.lihaoyi" %% "fastparse" % "0.2.1",
  "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.4",
 "org.scalatest" % "scalatest_2.11" % "2.2.4" % "test"
)
